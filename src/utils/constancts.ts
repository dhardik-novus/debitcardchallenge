import {Dimensions} from 'react-native';

export const SCREEN_WIDTH: number = Dimensions.get('window').width;
export const SCREEN_HEIGHT: number = Dimensions.get('window').height;

export const HEIGHT_PERG = (per: number): number => {
  return SCREEN_HEIGHT * per;
};
