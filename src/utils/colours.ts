export enum ThemeColours {
  primaryColor = '#0f3659',
  subPrimaryColor = '#ffffff',
  secondaryColor = '#21cf6b',
  subSecondaryColor = '#eeeeee',
  infoColor = '#000000',
  subInfoColor = '#22222266',
}
