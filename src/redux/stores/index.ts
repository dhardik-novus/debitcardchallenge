import {compose, applyMiddleware, createStore} from 'redux';
import reducers from '../reducers';
import createSagaMiddleware from 'redux-saga';
import rootSaga from '../../sagas';

// Create middleware of redux saga
const sagaMiddleware = createSagaMiddleware();
const middlewares = [sagaMiddleware];

const enhancer = compose(applyMiddleware(...middlewares));

// Create store for redux
const store = createStore(reducers, enhancer);

// Added root saga in middleware
sagaMiddleware.run(rootSaga);

export default store;
