export enum debitCardTypes {
  CHANGE_WEEKLY_LIMIT = 'CHANGE_WEEKLY_LIMIT',
  RESET_WEEKLY_LIMIT = 'RESET_WEEKLY_LIMIT',
  UPDATE_REQUEST_WEEKLY_LIMIT = 'UPDATE_REQUEST_WEEKLY_LIMIT',
}

export const updateWeekly = (payload: any) => ({
  type: debitCardTypes.UPDATE_REQUEST_WEEKLY_LIMIT,
  payload: payload,
});

export const setWeeklyLimit = (limit: any) => ({
  type: debitCardTypes.CHANGE_WEEKLY_LIMIT,
  payload: limit,
});

export const resetWeeklyLimit = () => ({
  type: debitCardTypes.RESET_WEEKLY_LIMIT,
  payload: '',
});
