import debitcard from './debitcard';
import {combineReducers} from 'redux';

const reducers = combineReducers({
  debitcard,
});

export default reducers;
