import {debitCardTypes} from '../../actions/debitCardAction';

interface Action {
  type: string;
  payload: string;
}

interface State {
  weeklyLimit: string;
}

export const intialState = {
  weeklyLimit: '',
};

export default (state: State = intialState, action: Action) => {
  switch (action.type) {
    case debitCardTypes.CHANGE_WEEKLY_LIMIT:
      return {
        ...state,
        weeklyLimit: action.payload,
      };
    case debitCardTypes.RESET_WEEKLY_LIMIT:
      return {
        ...state,
        weeklyLimit: '',
      };
    default:
      return state;
  }
};
