import * as React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import {ThemeColours} from '../../utils/colours';
import {HEIGHT_PERG} from '../../utils/constancts';
import BackgroundComponent from '../debitcard/components/BackgroundComponent';
import FastImage from 'react-native-fast-image';
import PriceComponent from './components/PriceComponet';

interface Props {
  goBack: () => void;
  setWeeklyValue: (value: string) => void;
}

const CreditLimitScreen: React.FC<Props> = ({goBack, setWeeklyValue}) => {
  const [text, onChangeText] = React.useState('');

  const setPriceValue = (value: string) => {
    onChangeText(value);
  };

  return (
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <View style={styles.mainContainer}>
        <BackgroundComponent
          back={true}
          title={'Spending Limit'}
          screen_name={'creditlimit'}
          goBack={goBack}
        />
        <View style={styles.foregroundContainer}>
          <View style={styles.infoContainter}>
            <FastImage
              style={styles.icon}
              source={require('../../assets/limit_small.png')}
              resizeMode={FastImage.resizeMode.contain}
            />
            <Text style={styles.titleInfo}>
              {'Set a weekly debit card spending limit'}
            </Text>
          </View>
          <View style={styles.inputContainer}>
            <Text style={styles.balanceSymbol}>{'S$'}</Text>
            <TextInput
              style={styles.input}
              onChangeText={onChangeText}
              value={text}
              keyboardType={'number-pad'}
            />
          </View>
          <Text style={styles.titleSubInfo}>
            {'Here weekly means the last 7 days - not the calendar week'}
          </Text>
          <View style={styles.pricesContainer}>
            <PriceComponent title="5000" onCallback={setPriceValue} />
            <PriceComponent title="10000" onCallback={setPriceValue} />
            <PriceComponent title="20000" onCallback={setPriceValue} />
          </View>
          <TouchableWithoutFeedback
            disabled={text.length === 0}
            onPress={() => setWeeklyValue(text)}>
            <View
              style={[
                styles.saveContainer,
                {
                  backgroundColor:
                    text.length > 0
                      ? ThemeColours.secondaryColor
                      : ThemeColours.subSecondaryColor,
                },
              ]}>
              <Text style={styles.saveTitle}>Save</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default CreditLimitScreen;

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  foregroundContainer: {
    marginTop: HEIGHT_PERG(0.2),
    height: HEIGHT_PERG(0.8),
    backgroundColor: ThemeColours.subPrimaryColor,
    borderTopRightRadius: 25,
    borderTopLeftRadius: 25,
    paddingHorizontal: 24,
  },
  infoContainter: {
    marginTop: 33,
    flexDirection: 'row',
    alignItems: 'center',
  },
  icon: {
    width: 16,
    height: 16,
  },
  titleInfo: {
    fontSize: 14,
    marginLeft: 10,
  },
  inputContainer: {
    marginTop: 10,
    flexDirection: 'row',
    paddingBottom: 5,
    borderBottomWidth: 1,
    borderColor: ThemeColours.subSecondaryColor,
  },
  balanceSymbol: {
    paddingHorizontal: 15,
    paddingVertical: 5,
    borderRadius: 5,
    overflow: 'hidden',
    fontSize: 15,
    fontWeight: '800',
    backgroundColor: ThemeColours.secondaryColor,
    color: ThemeColours.subPrimaryColor,
    alignSelf: 'center',
  },
  input: {
    flex: 1,
    margin: 12,
    fontSize: 25,
    fontWeight: '800',
  },
  titleSubInfo: {
    marginTop: 20,
    fontSize: 12,
    color: ThemeColours.subInfoColor,
  },
  pricesContainer: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  saveContainer: {
    position: 'absolute',
    bottom: 100,
    left: 50,
    right: 50,
    height: 56,
    borderRadius: 28,
    justifyContent: 'center',
    alignContent: 'center',
    backgroundColor: ThemeColours.secondaryColor,
  },
  saveTitle: {
    alignSelf: 'center',
    fontSize: 16,
    fontWeight: '600',
    color: ThemeColours.subPrimaryColor,
  },
});
