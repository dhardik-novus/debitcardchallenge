import * as React from 'react';
import {StyleSheet, Text, View, TouchableWithoutFeedback} from 'react-native';
import {ThemeColours} from '../../../utils/colours';

interface Props {
  title: string;
  onCallback: (value: string) => void;
}

const PriceComponent: React.FC<Props> = ({title, onCallback}) => {
  return (
    <TouchableWithoutFeedback onPress={() => onCallback(title)}>
      <View style={styles.container}>
        <Text style={styles.title}>S$ {title}</Text>
      </View>
    </TouchableWithoutFeedback>
  );
};

export default PriceComponent;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 25,
    paddingVertical: 12,
    backgroundColor: '#20D16707',
  },
  title: {
    fontSize: 14,
    textAlign: 'left',
    color: ThemeColours.secondaryColor,
  },
});
