import * as React from 'react';
import {ScrollView, StyleSheet, View, Modal, Text} from 'react-native';
import {ThemeColours} from '../../utils/colours';
import {HEIGHT_PERG} from '../../utils/constancts';
import CreditLimitScreen from '../creditlimit/CreditLimitScreen';
import BackgroundComponent from './components/BackgroundComponent';
import CardComponent from './components/CardComponet';
import OptionItemComponent from './components/OptionItemComponent';
import {ProgressBar} from 'react-native-paper';
import {useDispatch, useSelector} from 'react-redux';
import {updateWeekly} from '../../redux/actions/debitCardAction';

interface Props {}

const DebitCardScreen: React.FC<Props> = () => {
  const [isShow, onChangeShowCardNumber] = React.useState(false);
  const [modalVisible, setModalVisible] = React.useState(false);
  const [isFreeze] = React.useState('');
  const weeklyValue = useSelector(state => state.debitcard.weeklyLimit);
  const dispatch = useDispatch();

  const updateWeeklyLimit = (value: boolean): void => {
    if (value) {
      setModalVisible(true);
    } else {
      setWeeklyValue('');
    }
  };

  const closeModal = () => {
    setModalVisible(false);
  };

  const setWeeklyValue = (value: string) => {
    dispatch(updateWeekly(value));
    closeModal();
  };

  return (
    <View style={styles.mainContainer}>
      <BackgroundComponent title="Debit Card" screen_name="debitcard" />
      <ScrollView
        contentContainerStyle={styles.foregroundContainer}
        showsVerticalScrollIndicator={false}>
        <View style={styles.foregroundInnerContainer}>
          {weeklyValue.length > 0 && (
            <View>
              <View style={styles.progressInnerContainer}>
                <Text>Debit card spending limit</Text>
                <Text style={{color: ThemeColours.secondaryColor}}>
                  $345 |{' '}
                  <Text style={{color: ThemeColours.subInfoColor}}>$5,000</Text>
                </Text>
              </View>
              <ProgressBar
                style={styles.progressBar}
                progress={0.3}
                color={ThemeColours.secondaryColor}
              />
            </View>
          )}
          <OptionItemComponent
            name={'Top-up account'}
            sub_title={'Deposit money to your account to use with card'}
            icon={require('../../assets/insight.png')}
            isSwitch={false}
          />
          <OptionItemComponent
            name={'Weekly spending limit'}
            sub_title={'You haven’t set any spending limit on card'}
            icon={require('../../assets/limit.png')}
            isSwitch={true}
            switchValue={weeklyValue}
            onChangeSwitchAction={updateWeeklyLimit}
          />
          <OptionItemComponent
            name={'Freeze card'}
            sub_title={'Your debit card is currently active'}
            icon={require('../../assets/freeze.png')}
            switchValue={isFreeze}
            isSwitch={true}
          />
          <OptionItemComponent
            name={'Get a new card'}
            sub_title={'This deactivates your current debit card'}
            icon={require('../../assets/add_card.png')}
            isSwitch={false}
          />
          <OptionItemComponent
            name={'Deactivated cards'}
            sub_title={'Your previously deactivated cards'}
            icon={require('../../assets/deactive.png')}
            isSwitch={false}
          />
        </View>
        <CardComponent
          isShow={isShow}
          onChangeShowAction={onChangeShowCardNumber}
        />
      </ScrollView>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}>
        <CreditLimitScreen
          goBack={closeModal}
          setWeeklyValue={setWeeklyValue}
        />
      </Modal>
    </View>
  );
};

export default DebitCardScreen;

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  foregroundContainer: {
    marginTop: HEIGHT_PERG(0.25),
    paddingTop: HEIGHT_PERG(0.1),
  },
  foregroundInnerContainer: {
    minHeight: HEIGHT_PERG(1),
    backgroundColor: ThemeColours.subPrimaryColor,
    borderTopRightRadius: 25,
    borderTopLeftRadius: 25,
    marginTop: 0,
    paddingTop: HEIGHT_PERG(0.2),
    paddingHorizontal: 24,
  },
  progressInnerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  progressBar: {
    height: 16,
    borderRadius: 8,
    marginTop: 10,
    marginBottom: 10,
  },
});
