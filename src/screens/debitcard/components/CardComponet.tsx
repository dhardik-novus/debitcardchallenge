import * as React from 'react';
import {StyleSheet, Text, View, TouchableWithoutFeedback} from 'react-native';
import {ThemeColours} from '../../../utils/colours';
import FastImage from 'react-native-fast-image';

interface Props {
  isShow: boolean;
  onChangeShowAction: React.Dispatch<React.SetStateAction<boolean>>;
}

const CardComponent: React.FC<Props> = ({isShow, onChangeShowAction}) => {
  return (
    <View style={styles.container}>
      <TouchableWithoutFeedback onPress={() => onChangeShowAction(!isShow)}>
        <View style={styles.buttonVisibleContainer}>
          <FastImage
            style={styles.eyeIcon}
            source={
              isShow
                ? require('../../../assets/red_eye.png')
                : require('../../../assets/remove_red_eye.png')
            }
            resizeMode={FastImage.resizeMode.contain}
          />
          <Text style={styles.titleCardNumber}>
            {isShow ? 'Hide' : 'Show'}
            {' card number'}
          </Text>
        </View>
      </TouchableWithoutFeedback>
      <View style={styles.cardContainer}>
        <FastImage
          style={styles.aspireLogo}
          source={require('../../../assets/aspire_logo.png')}
          resizeMode={FastImage.resizeMode.contain}
        />
        <Text style={styles.titleName}>{'Mark Henry'}</Text>
        {isShow ? (
          <Text
            style={[
              styles.titleHideCardNumber,
              {marginTop: 20, marginBottom: 6.5},
            ]}>
            {'5 6 4 7    3 4 1 1    2 4 1 3    2 0 2 0'}
          </Text>
        ) : (
          <Text style={styles.titleShowCardNumber}>
            {'••••    ••••    ••••    '}
            <Text style={styles.titleHideCardNumber}>{'2020'}</Text>
          </Text>
        )}
        <View style={{flexDirection: 'row'}}>
          <Text style={styles.titleThru}>{'Thru: 12/24'}</Text>
          <Text style={[styles.titleThru, {marginLeft: 40}]}>
            {isShow ? 'CVV: 123' : 'CVV: * * *'}
          </Text>
        </View>
        <FastImage
          style={styles.visaLogo}
          source={require('../../../assets/visa_logo.png')}
          resizeMode={FastImage.resizeMode.contain}
        />
      </View>
    </View>
  );
};

export default CardComponent;

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    zIndex: 0,
    paddingHorizontal: 24,
  },
  buttonVisibleContainer: {
    alignSelf: 'flex-end',
    width: 180,
    height: 55,
    paddingTop: 10,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: ThemeColours.subPrimaryColor,
  },
  eyeIcon: {
    width: 16,
    height: 16,
  },
  titleCardNumber: {
    marginLeft: 8,
    fontSize: 13,
    fontWeight: '500',
    color: ThemeColours.secondaryColor,
  },
  cardContainer: {
    height: 210,
    backgroundColor: ThemeColours.secondaryColor,
    borderRadius: 20,
    marginTop: -20,
    padding: 20,
  },
  aspireLogo: {
    alignSelf: 'flex-end',
    width: 74,
    height: 21,
  },
  titleName: {
    marginTop: 20,
    fontSize: 24,
    fontWeight: '800',
    color: ThemeColours.subPrimaryColor,
  },
  titleHideCardNumber: {
    marginTop: 10,
    fontSize: 16,
    fontWeight: '600',
    color: ThemeColours.subPrimaryColor,
    alignItems: 'center',
  },
  titleShowCardNumber: {
    marginTop: 10,
    fontSize: 30,
    fontWeight: '600',
    color: ThemeColours.subPrimaryColor,
    alignItems: 'center',
  },
  titleThru: {
    marginTop: 10,
    fontSize: 14,
    fontWeight: '500',
    color: ThemeColours.subPrimaryColor,
  },
  visaLogo: {
    alignSelf: 'flex-end',
    width: 59,
    height: 20,
  },
});
