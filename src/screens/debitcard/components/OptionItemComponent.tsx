import * as React from 'react';
import {StyleSheet, Text, View, Switch} from 'react-native';
import {ThemeColours} from '../../../utils/colours';
import FastImage from 'react-native-fast-image';

interface Props {
  name: string;
  sub_title: string;
  icon: NodeRequire;
  isSwitch: boolean;
  switchValue?: string;
  onChangeSwitchAction?: (value: boolean) => void;
}

const OptionItemComponent: React.FC<Props> = props => {
  const toggleSwitch = () => {
    props.onChangeSwitchAction(!(props.switchValue?.length !== 0));
  };

  return (
    <View style={styles.container}>
      <FastImage
        style={styles.icon}
        source={props.icon}
        resizeMode={FastImage.resizeMode.contain}
      />
      <View style={styles.titleContainer}>
        <Text style={styles.title}>{props.name}</Text>
        <Text style={styles.subTitle}>{props.sub_title}</Text>
      </View>
      {props.isSwitch && (
        <Switch
          trackColor={{
            false: ThemeColours.subSecondaryColor,
            true: ThemeColours.secondaryColor,
          }}
          thumbColor={ThemeColours.subPrimaryColor}
          ios_backgroundColor={ThemeColours.subSecondaryColor}
          onValueChange={toggleSwitch}
          value={props.switchValue?.length !== 0}
        />
      )}
    </View>
  );
};

export default OptionItemComponent;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 15,
  },
  icon: {
    width: 32,
    height: 32,
  },
  titleContainer: {
    flex: 1,
    marginLeft: 15,
  },
  title: {
    fontSize: 14,
    textAlign: 'left',
    color: ThemeColours.infoColor,
  },
  subTitle: {
    marginTop: 2,
    fontSize: 12,
    textAlign: 'left',
    color: ThemeColours.infoColor,
    opacity: 0.4,
  },
});
