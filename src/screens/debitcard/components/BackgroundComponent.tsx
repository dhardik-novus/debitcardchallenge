import * as React from 'react';
import {StyleSheet, Text, View, TouchableWithoutFeedback} from 'react-native';
import {ThemeColours} from '../../../utils/colours';
import {HEIGHT_PERG} from '../../../utils/constancts';
import FastImage from 'react-native-fast-image';
import Icon from 'react-native-vector-icons/Ionicons';

interface Props {
  back?: boolean;
  title: string;
  screen_name?: string;
  goBack?: (() => void) | undefined;
}

const BackgroundComponent: React.FC<Props> = ({
  back,
  title,
  screen_name,
  goBack,
}) => {
  return (
    <View style={styles.container}>
      {back && (
        <TouchableWithoutFeedback onPress={goBack}>
          <View style={styles.backIcon}>
            <Icon
              name={'chevron-back'}
              size={35}
              color={ThemeColours.subPrimaryColor}
            />
          </View>
        </TouchableWithoutFeedback>
      )}
      <FastImage
        style={styles.icon}
        source={require('../../../assets/small_logo.png')}
        resizeMode={FastImage.resizeMode.contain}
      />
      <Text style={styles.title}>{title}</Text>
      {screen_name === 'debitcard' && (
        <>
          <Text style={styles.balanceTitle}>Available Balance</Text>
          <View style={styles.balanceView}>
            <Text style={styles.balanceSymbol}>{'S$'}</Text>
            <Text style={styles.balanceAmount}>{'3,000'}</Text>
          </View>
        </>
      )}
    </View>
  );
};

export default BackgroundComponent;

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    height: HEIGHT_PERG(1),
    backgroundColor: ThemeColours.primaryColor,
    paddingHorizontal: 24,
    paddingVertical: 75,
  },
  backIcon: {
    position: 'absolute',
    left: 24,
    top: 50,
  },
  icon: {
    position: 'absolute',
    right: 24,
    top: 50,
    width: 27,
    height: 27,
  },
  title: {
    marginTop: 20,
    fontSize: 25,
    fontWeight: '800',
    textAlign: 'left',
    color: ThemeColours.subPrimaryColor,
  },
  balanceTitle: {
    marginTop: 30,
    fontSize: 13,
    fontWeight: '500',
    textAlign: 'left',
    color: ThemeColours.subPrimaryColor,
  },
  balanceView: {
    marginTop: 10,
    flexDirection: 'row',
  },
  balanceSymbol: {
    paddingHorizontal: 15,
    paddingVertical: 5,
    borderRadius: 5,
    overflow: 'hidden',
    fontSize: 15,
    fontWeight: '800',
    backgroundColor: ThemeColours.secondaryColor,
    color: ThemeColours.subPrimaryColor,
  },
  balanceAmount: {
    marginLeft: 12,
    fontSize: 25,
    fontWeight: '800',
    color: ThemeColours.subPrimaryColor,
  },
});
