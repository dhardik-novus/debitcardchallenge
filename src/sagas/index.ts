import {all, fork} from 'redux-saga/effects';
import debitCardSaga from './debitCardSaga';

function* rootSaga() {
  yield all([fork(debitCardSaga)]);
}

export default rootSaga;
