import {put, takeLatest} from 'redux-saga/effects';
import {
  debitCardTypes,
  resetWeeklyLimit,
  setWeeklyLimit,
} from '../redux/actions/debitCardAction';

function* updateWeekly(payload: any) {
  try {
    yield put(setWeeklyLimit(payload.payload));
  } catch (error) {
    yield put(resetWeeklyLimit());
  }
}

export default function* debitCardSaga() {
  yield takeLatest(debitCardTypes.UPDATE_REQUEST_WEEKLY_LIMIT, updateWeekly);
}
