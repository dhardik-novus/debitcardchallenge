import React from 'react';
import {StatusBar} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import TabNavigation from './TabNavigation';

// @refresh reset
const AppNavigation = () => {
  return (
    <NavigationContainer>
      <StatusBar barStyle={'light-content'} />
      <TabNavigation />
    </NavigationContainer>
  );
};

export default AppNavigation;
