import React from 'react';
import {
  createBottomTabNavigator,
  BottomTabNavigationOptions,
} from '@react-navigation/bottom-tabs';
import HomeScreen from '../screens/home/HomeScreen';
import DebitCardScreen from '../screens/debitcard/DebitCardScreen';
import PaymentsScreen from '../screens/payments/PaymentsScreen';
import CreditScreen from '../screens/credit/CreditScreen';
import ProfileScreen from '../screens/profile/ProfileScreen';
import FastImage from 'react-native-fast-image';
import {ThemeColours} from '../utils/colours';

const Tab = createBottomTabNavigator();

const tabOptions: BottomTabNavigationOptions = {
  tabBarLabelStyle: {fontSize: 12},
  tabBarActiveTintColor: ThemeColours.secondaryColor,
  headerShown: false,
};

const iconSize = {
  width: 24,
  height: 24,
};

// @refresh reset
const TabNavigation = () => {
  return (
    <Tab.Navigator initialRouteName={'Debit Card'}>
      <Tab.Screen
        name="Home"
        component={HomeScreen}
        options={{
          ...tabOptions,
          tabBarIcon: () => (
            <FastImage
              style={iconSize}
              source={require('../assets/home.png')}
              resizeMode={FastImage.resizeMode.contain}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Debit Card"
        component={DebitCardScreen}
        options={{
          ...tabOptions,
          tabBarIcon: () => (
            <FastImage
              style={iconSize}
              source={require('../assets/pay.png')}
              resizeMode={FastImage.resizeMode.contain}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Payments"
        component={PaymentsScreen}
        options={{
          ...tabOptions,
          tabBarIcon: () => (
            <FastImage
              style={iconSize}
              source={require('../assets/payments.png')}
              resizeMode={FastImage.resizeMode.contain}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Credit"
        component={CreditScreen}
        options={{
          ...tabOptions,
          tabBarIcon: () => (
            <FastImage
              style={iconSize}
              source={require('../assets/credit.png')}
              resizeMode={FastImage.resizeMode.contain}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={ProfileScreen}
        options={{
          ...tabOptions,
          tabBarIcon: () => (
            <FastImage
              style={iconSize}
              source={require('../assets/account.png')}
              resizeMode={FastImage.resizeMode.contain}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default TabNavigation;
