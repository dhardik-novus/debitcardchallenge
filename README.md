# DebitCardChallenge

# Requirements

- [ ] Create new debit card.
- [ ] Manage debit card.
- [ ] You can deposit money using debit card.
- [ ] You can instantly freeze your debit card if you do not use it.
- [ ] You can instantly deacive your debit card if something went.  
- [ ] Manage your weekly expensive using set weekly limit.
- [ ] Nofity when your expensive is more than weekly limit.

## Debit Card Challenge

## Description
Using this application, people is easy to maintain their debit card. people can save the time without going to physical office.
This application help out of money deposit, notify when weekly expensive limit crossed, freeze your debit card and many more. 

## Installation

- Downloand the code 
- Go to project directory where you have download on it. (```$ cd ${PROJECT_DIRECTORY}```)
- ```$ yarn install```
  or 
  ```$ npm install```
- iOS run: ```$ npm run-ios```
  or 
  Android run: ```$ npm run-android```